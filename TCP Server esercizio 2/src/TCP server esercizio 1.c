#if defined WIN32
#include <winsock.h>
#else
#define closesocket close
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#endif
#include <stdio.h>
#include <stdlib.h> // for atoi()
#include <string.h>
#include <ctype.h>
#define PROTOPORT 5193 // default protocol port number
#define QLEN 6 // size of request queue
void ErrorHandler(char *errorMessage) {
	printf(errorMessage);
}
void ClearWinSock() {
#if defined WIN32
	WSACleanup();
#endif
}
int main(int argc, char *argv[]) {
	int port;
	if (argc > 1) {
		port = atoi(argv[1]); // if argument specified convert argument to binary
	}
	else
		port = PROTOPORT; // use default port number
	if (port < 0) {
		printf("bad port number %s \n", argv[1]);
		return 0;
	}
#if defined WIN32 // initialize Winsock
	WSADATA wsaData;
	int iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
	if (iResult != 0) {
		ErrorHandler("Error at WSAStartup()\n");
		return 0;
	}
#endif
	int MySocket;
	MySocket = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (MySocket < 0) {
		ErrorHandler("socket creation failed.\n");
		ClearWinSock();
		system("PAUSE");
		return 0;
	}
	// ASSEGNAZIONE DI UN INDIRIZZO ALLA SOCKET
	struct sockaddr_in sad;
	memset(&sad, 0, sizeof(sad)); // ensures that extra bytes contain 0
	sad.sin_family = AF_INET;
	sad.sin_addr.s_addr = inet_addr("127.0.0.1");
	sad.sin_port = htons(port); /* converts values between the host and
	network byte order. Specifically, htons() converts 16-bit quantities
	from host byte order to network byte order. */
	if (bind(MySocket, (struct sockaddr*) &sad, sizeof(sad)) < 0) {
		ErrorHandler("bind() failed.\n");
		closesocket(MySocket);
		ClearWinSock();
		system("PAUSE");
		return 0;
	}
	if (listen (MySocket, QLEN) < 0) {
		ErrorHandler("listen() failed.\n");
		closesocket(MySocket);
		ClearWinSock();
		system("PAUSE");
		return 0;
	}
	// ACCETTARE UNA NUOVA CONNESSIONE
	struct sockaddr_in cad; // structure for the client address
	int clientSocket; // socket descriptor for the client
	int clientLen; // the size of the client address
	printf("Waiting for a client to connect...\n");
	while (1) { /* oppure for (;;) */
		clientLen = sizeof(cad); // set the size of the client address
		clientSocket = accept(MySocket, (struct sockaddr *)&cad, &clientLen);
		if (clientSocket < 0) {
			ErrorHandler("accept() failed.\n");
			closesocket(MySocket);
			ClearWinSock();
			system("PAUSE");
			return 0;
		}
		printf("Connection established! Handling client %s:%d\n", inet_ntoa(cad.sin_addr),cad.sin_port);
		// RICEZIONE STRINGA 'Hello'
		char hello[10];
		int nbytesH=recv(clientSocket, hello, sizeof(hello)-1, 0);
		hello[nbytesH]='\0';
		if(nbytesH!=strlen(hello)) {
			ErrorHandler("recv() received less number of bytes than expected.\n");
			closesocket(MySocket);
			ClearWinSock();
			system("PAUSE");
			return 0;
		}
		char* connEst = malloc(20);
		connEst = "ack";
		int lenConn = strlen(connEst);
		if (send(clientSocket, connEst, lenConn, 0) != lenConn) {
			ErrorHandler("send() sent a different number of bytes than expected\n");
			closesocket(clientSocket);
			ClearWinSock();
			system("PAUSE");
			return 0;
		}
		// RICEZIONE DATI DAL CLIENT
		char num1[10]="";
		char num2[10]="";
		int nbytes1=recv(clientSocket, num1, sizeof(num1), 0);
		num1[nbytes1]='\0';
		if(nbytes1>sizeof(num1)) {
			ErrorHandler("first server recv() received less number of bytes than expected.\n");
			printf("Received: %s\n",num1);
			closesocket(MySocket);
			ClearWinSock();
			system("PAUSE");
			return 0;
		}
		int n1=(int)strtol(num1,NULL,10);
		int nbytes2=recv(clientSocket, num2, sizeof(num2), 0);
		num2[nbytes2]='\0';
		if(nbytes2>sizeof(num2)) {
			ErrorHandler("second server recv() received less number of bytes than expected.\n");
			printf("Received: %s\n",num1);
			closesocket(MySocket);
			ClearWinSock();
			system("PAUSE");
			return 0;
		}
		int n2=(int)strtol(num2,NULL,10);
		long somma=n1+n2;
		printf("La somma di %d + %d = %ld\n", n1, n2, somma);
		char *sum=malloc(sizeof(long)*sizeof(somma));
		snprintf(sum, sizeof(sum)*sizeof(long), "%ld", somma);
		// INVIO DATI AL CLIENT
		if (send(clientSocket, sum, sizeof(sum)*sizeof(long), 0)<0) {
			ErrorHandler("server send() sent a different number of bytes than expected\n");
			closesocket(clientSocket);
			ClearWinSock();
			system("PAUSE");
			return 0;
		}
	}
}
