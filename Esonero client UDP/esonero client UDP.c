/*
 * esonero client UDP.c
 *
 *  Created on: 18/nov/2017
 *      Author: Manosperta Luigi - 652735
 */

#if defined WIN32
#include <winsock.h>
#else
#define closesocket close
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
#endif
#include <stdio.h>
#include <string.h>
#define MAX 255 //numero massimo di caratteri da inserire

struct msg { //struttura per inviare le stringhe e ricevere la concatenazione
	char str1[100];
	char str2[100];
	char reply[200];
};

void ErrorHandler(char *errorMessage) {
	printf(errorMessage);
}

void ClearWinSock() {
#if defined WIN32
	WSACleanup();
#endif
}

int main(void) {
#if defined WIN32
	WSADATA wsaData; //žnizializza la winsock
	int iResult = WSAStartup(MAKEWORD(2, 2), &wsaData); //specifica la versione di socket e i dettagli implementativi della stessa
	if (iResult != 0) {
		printf("error at WSASturtup\n");
		return 0;
	}
#endif
	int sock; //descrittore della socket
	struct sockaddr_in server; //struttura del server
	struct sockaddr_in client; //struttura del client
	char nameServer[512];
	int port;
	char str1[MAX], str2[MAX];
	char recvd[MAX];
	int recvd_length;
	printf("Inserire il nome del server: ");
	scanf("%s", nameServer);
	if (strlen(nameServer) > 512)
		ErrorHandler("Il nome del DNS inserito \212 troppo lungo");
	printf("Inserire il numero di porta: ");
	scanf("%d", &port);
	struct hostent *host;
	host = gethostbyname(nameServer); //effettua la risoluzione DNS da nome simbolico a indirizzo IP
	struct in_addr* ina = (struct in_addr*)host->h_addr_list[0];
	// CREAZIONE DELLA SOCKET
	if ((sock = socket(AF_INET, SOCK_DGRAM, 0)) < 0) { //creazione di una socket UDP (servizio non orientato alla connessione)
		ErrorHandler("socket() failed");
	}
	// COSTRUZIONE DELL'INDIRIZZO DEL SERVER
	memset(&server, 0, sizeof(struct sockaddr_in)); //alloca e inizializza la struttura del server
	server.sin_family = AF_INET; //address family 16 bit
	server.sin_addr = *ina; //recupera lžindirizzo IP corrispondente al nome simbolico inserito dapprima via stdin
	server.sin_port = htons(port);
	if (sendto(sock, "hello", strlen(str1), 0, (struct sockaddr*) &server, sizeof(struct sockaddr_in)) < 0) { //invio 'hello'
		ErrorHandler("sendto() ha generato un numero di bytes differenti\n");
	}
	unsigned int fromSize = sizeof(struct sockaddr_in);
	int messRicevuto = recvfrom(sock, recvd, MAX, 0, (struct sockaddr*) &client, (int*)&fromSize); //ricezione 'messaggio ricevuto'
	if (server.sin_addr.s_addr != client.sin_addr.s_addr) { //verifica se chi ha inviato il pacchetto sia il vero server
		ErrorHandler("Errore: numero di bytes diverso rispetto alla stringa 'messaggio ricevuto'\n");
	}
	recvd[messRicevuto]='\0';
	printf("%s\n",recvd);
	struct msg *mess = malloc(sizeof(struct msg)); //allocazione della struct come puntatore
	do {
		int length1 = 0, length2 = 0;
		strcpy(str1, "");
		strcpy(str2, "");
		strcpy(recvd, "");
		do {
			printf("Inserire prima stringa: ");
			scanf("%s", str1);
			length1 = strlen(str1);
		} while(length1 > 100);
		do{
			printf("Inserire seconda stringa: ");
			scanf("%s", str2);
			length2 = strlen(str2);
		} while(length2 > 100);
		str1[strlen(str1)] = '\0';
		str2[strlen(str2)] = '\0';
		strcpy(mess->str1,str1);
		strcpy(mess->str2,str2);
		strcpy(mess->reply,"");
		if (sendto(sock, mess, sizeof(*mess), 0, (struct sockaddr*) &server, sizeof(struct sockaddr_in)) < 0) { //invio del tutto
			ErrorHandler("sendto() ha generato un numero di bytes differenti\n");
			break;
		}
		recvd_length = recvfrom(sock, mess, sizeof(*mess), 0, (struct sockaddr*) &client, &fromSize); //ricezione dell'elaborazione
		if (server.sin_addr.s_addr != client.sin_addr.s_addr) {
			fprintf(stderr, "Errore: numero di bytes diverso rispetto alla stringa ricevuta\n");
			exit(EXIT_FAILURE);
		}
		printf("La concatenazione di %s e %s \212: %s\n", str1, str2, mess->reply);
	} while((strcmp(mess->reply,"bye")) != 0);
	closesocket(sock);
	ClearWinSock();
	system("PAUSE");
	return EXIT_SUCCESS;
}
