/*
 * esonero server UDP.c
 *
 *  Created on: 18/nov/2017
 *      Author: Manosperta Luigi - 652735
 */
#if defined WIN32
#include <winsock.h>
#else
#define closesocket close
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
#endif
#include <stdio.h>
#include <string.h>
#include<stdlib.h>
#include<stdio.h>
#define MAX 255
#define PORT 60000

struct msg {
	char str1[100];
	char str2[100];
	char reply[200];
};

void ErrorHandler(char *errorMessage) {
	printf(errorMessage);
}

void ClearWinSock() {
#if defined WIN32
	WSACleanup();
#endif
}

int main() {
#if defined WIN32
	WSADATA wsaData;
	int iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);  //specifica la versione di socket e i dettagli implementativi della stessa
	if (iResult != 0) {
		printf("error at WSASturtup\n");
		return 0;
	}
#endif
	int sock; //descrittore della socket
	struct sockaddr_in server; //struttura del server
	struct sockaddr_in client; //struttura del client
	// CREAZIONE DELLA SOCKET
	if ((sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0) { //creazione di una socket UDP (servizio non orientato alla connessione)
		ErrorHandler("socket() failed\n");
	}
	// COSTRUZIONE DELL'INDIRIZZO DEL SERVER
	memset(&server, 0, sizeof(server)); //alloca e inizializza la struttura del server
	server.sin_family = AF_INET; //address family 16 bit
	server.sin_port = htons(PORT);
	server.sin_addr.s_addr = inet_addr("127.0.0.1");
	// BIND DELLA SOCKET
	if ((bind(sock, (struct sockaddr *) &server, sizeof(server))) < 0) { //associa alla socket in indirizzo IP e un numero di porta
		ErrorHandler("bind() failed");
	}
	unsigned int clientLength = sizeof(struct sockaddr_in);
	struct msg *mess = malloc(sizeof(struct msg));
	while (1) {
		printf("Waiting for a connection...\n");
		struct hostent *host;
		char buffer[5];
		int lung;
		if ((lung = recvfrom(sock, buffer, MAX, 0, (struct sockaddr *) &client, (int*)&clientLength)) < 0) { //ricezione di 'hello'
			ErrorHandler("recvfrom() della stringa 'hello' ha generato un errore\n");
		}
		buffer[lung]='\0';
		host = gethostbyaddr((char *) &client.sin_addr, sizeof(client.sin_addr), AF_INET); //effettua la risoluzione da indirizzo Internet a nome simbolico
		char *nome_canonico = host->h_name;
		printf("Stringa 'hello' ricevuta dal client: %s\n", nome_canonico);
		if (sendto(sock, "messaggio ricevuto", strlen("messaggio ricevuto"), 0, (struct sockaddr *) &client, sizeof(struct sockaddr)) <0) { //invio 'messaggio ricevuto'
			ErrorHandler("sendto() della stringa 'messaggio ricevuto' ha generato un errore\n");
		}
		do {
			int ricev1 = 0;
			if ((ricev1 = recvfrom(sock, mess, sizeof(*mess), 0, (struct sockaddr *) &client, &clientLength)) < 0) { //ricezione stringhe
				ErrorHandler("recvfrom() ha generato un errore\n");
			}
			if ((strcmp(mess->str1, "quit") == 0) | (strcmp(mess->str2, "quit") == 0)) {
				strcpy(mess->reply, "bye");
			} else {
				strcpy(mess->reply, mess->str1);
				strcat(mess->reply, mess->str2);
			}
			printf("%s + %s = %s\n",mess->str1, mess->str2, mess->reply);
			if (sendto(sock, mess, sizeof(*mess), 0, (struct sockaddr *) &client, sizeof(struct sockaddr)) <0) {
				ErrorHandler("sendto() non ha rispettato il numero di bytes\n");
				printf("-----------------------------------------------------------\n");
			}
		} while((strcmp(mess->reply,"bye")) != 0);
	}
}
