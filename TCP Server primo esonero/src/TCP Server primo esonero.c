/*
 * TCP Server primo esonero.c
 *
 *  Created on: 20 ott 2017
 *      Author: Manosperta Luigi, matricola 652735
 */

#if defined WIN32
#include <winsock.h> //libreria per la gestione delle socket in C
#else
#define closesocket close
#include <sys/socket.h>
#include <arpa/inet.h> //protocolli Internet
#include <unistd.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>
#define PROTOPORT 5193 //porta pre-impostata su cui il server rimarr� in ascolto
#define QLEN 10000 //la traccia richiede connessioni teoricamente infinite
void ErrorHandler(char *errorMessage) {
	printf(errorMessage);
}
void ClearWinSock() {
#if defined WIN32
	WSACleanup();
#endif
}

typedef struct { //uso la struct come da traccia per trasferire stringhe in input e risultato della concatenazione
	char msg1[100];
	char msg2[100];
	char msg3[200];
} message;

int main(int argc, char *argv[]) {
	int port;
	if (argc>1) {
		port=atoi(argv[1]);
	} else
		port=PROTOPORT; //se la porta non � specificata in esecuzione, allora uso quella di default
	if (port < 0) {
		printf("bad port number: %s\n", argv[1]);
		return 0;
	}
#if defined WIN32 //inizializzazione della winsock
	WSADATA wsaData;
	int iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
	if (iResult != 0) {
		ErrorHandler("Error at WSAStartup()\n");
		return 0;
	}
#endif
	int MySocket;
	MySocket = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP); //creazione della socket di benvenuto lato server
	if (MySocket < 0) {
		ErrorHandler("socket creation failed.\n");
		ClearWinSock();
		system("PAUSE");
		return 0;
	}
	struct sockaddr_in sad; //struttura che ingloba la definizione di socket server
	memset(&sad, 0, sizeof(sad));
	sad.sin_family = AF_INET; //protocollo Internet corrispondente all'Internet Protocol v4
	sad.sin_addr.s_addr = inet_addr("127.0.0.1"); //il server � in localhost
	sad.sin_port = htons(port); //converte un numero decimale a 16 bit in formato Big-Endian per la rete
	if (bind(MySocket, (struct sockaddr*) &sad, sizeof(sad)) < 0) { //si associa alla socket la struttura
		ErrorHandler("bind() failed.\n");
		closesocket(MySocket);
		ClearWinSock();
		system("PAUSE");
		return 0;
	}
	if (listen (MySocket, QLEN) < 0) { //il server rimane in ascolto teoricamente all'infinito
		ErrorHandler("listen() failed.\n");
		closesocket(MySocket);
		ClearWinSock();
		system("PAUSE");
		return 0;
	}
	struct sockaddr_in cad; //struttura che ingloba la definizione della socket del client connesso al server
	int clientSocket; //descrittore della socket del client
	int clientLen;
	printf("Waiting for a client to connect...\n");
	message msgs;
	while(1) { //rimarr� sempre in ascolto a ciclare qui
		clientLen = sizeof(cad); //grandezza della struttura del client
		clientSocket = accept(MySocket, (struct sockaddr *)&cad, &clientLen); //accetta la connessione di un client
		if (clientSocket < 0) {
			ErrorHandler("accept() failed.\n");
			closesocket(MySocket);
			ClearWinSock();
			system("PAUSE");
			return 0;
		}
		printf("Connessione stabilita con il client: %s:%d\n", inet_ntoa(cad.sin_addr),cad.sin_port);
		//INVIO STRINGA 'connessione avvenuta'
		char* connEst = malloc(20);
		connEst = "connessione avvenuta";
		int lenConn = strlen(connEst);
		if (send(clientSocket, connEst, lenConn, 0) <0) {
			ErrorHandler("first send() sent a different number of bytes than expected\n");
			closesocket(clientSocket);
			ClearWinSock();
			system("PAUSE");
			return 0;
		}
		while(1){ //qui, come da traccia, il server rimarr� in ascolto per tutte le volte possibili
			boolean goOn=false; //usato per breakare il loop infinito se si riceve 'quit' in una delle due stringhe
			//piccolo reset artigianale della memoria
			strcpy(msgs.msg1,"");
			strcpy(msgs.msg2,"");
			strcpy(msgs.msg3,"");
			// RICEZIONE STRINGHE DAL CLIENT
			int nbytes1=recv(clientSocket, (void *)&msgs, sizeof(msgs), 0);
			if(nbytes1>sizeof(msgs)) {
				ErrorHandler("first recv() received less number of bytes than expected.\n");
				closesocket(MySocket);
				ClearWinSock();
				system("PAUSE");
				return 0;
			}
			if(strcmp(msgs.msg1,"quit")!=0) {
				strcpy(msgs.msg3,msgs.msg1);
				strcat(msgs.msg3,msgs.msg2);
			} else {
				strcpy(msgs.msg3,"bye");
				goOn=true;
			}
			if(strcmp(msgs.msg2,"quit")!=0) {
				strcpy(msgs.msg3,msgs.msg1);
				strcat(msgs.msg3,msgs.msg2);
			} else {
				strcpy(msgs.msg3,"bye");
				goOn=true;
			}
			printf("%s + %s = %s\n",msgs.msg1, msgs.msg2, msgs.msg3);
			if (send(clientSocket, (void *)&msgs, sizeof(msgs), 0) <0) { //invio della concatenazione
				ErrorHandler("second send() sent a different number of bytes than expected\n");
				closesocket(clientSocket);
				ClearWinSock();
				system("PAUSE");
				return 0;
			}
			if(goOn) //se una delle due stringhe era 'quit', questo loop termina in attesa di un nuovo client
				break;
		}
	}
}
