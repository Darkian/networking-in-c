#if defined WIN32
#include <winsock.h>
#else
#define closesocket close
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#define BUFFERSIZE 512
#define PROTOPORT 5193 // Numero di porta di default

void ErrorHandler(char *errorMessage) {
	printf(errorMessage);
}

void ClearWinSock() {
#if defined WIN32
	WSACleanup();
#endif

}

int main(void) {
#if defined WIN32
	WSADATA wsaData;
	int iResult = WSAStartup(MAKEWORD(2 ,2), &wsaData);
	if (iResult != 0) {
		printf ("error at WSASturtup\n");
		system("PAUSE");
		return 0;
	}
#endif
	// CREAZIONE DELLA SOCKET
	int Csocket;
	Csocket = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (Csocket < 0) {
		ErrorHandler("socket creation failed.\n");
		closesocket(Csocket);
		ClearWinSock();
		system("PAUSE");
		return 0;
	}
	// COSTRUZIONE DELL�INDIRIZZO DEL SERVER
	struct sockaddr_in sad;
	memset(&sad, 0, sizeof(sad));
	sad.sin_family = AF_INET;
	sad.sin_addr.s_addr = inet_addr("127.0.0.1"); // IP del server
	sad.sin_port = htons(5193); // Server port
	// CONNESSIONE AL SERVER
	if (connect(Csocket, (struct sockaddr *)&sad, sizeof(sad)) < 0) {
		ErrorHandler( "Failed to connect.\n" );
		closesocket(Csocket);
		ClearWinSock();
		system("PAUSE");
		return 0;
	}
	// RICEZIONE DELLA STRINGA DI AVVENUTA CONNESSIONE
	char connEst[512]="";
	int nbytesEst=recv(Csocket, connEst, sizeof(connEst)-1, 0);
	connEst[nbytesEst]='\0';
	printf("Received: %s\n",connEst);
	if(nbytesEst!=strlen(connEst)) {
		ErrorHandler("recv() received less number of bytes than expected.\n");
		printf("Received: << %s >>\n---Bytes: %d | Error: %d ---\n",connEst, nbytesEst, errno);
		closesocket(Csocket);
		ClearWinSock();
		system("PAUSE");
		return 0;
	}
	char* inputString1 = malloc(512);
	printf("Inserire prima parola senza spazi: ");
	scanf("%s", inputString1);
	int stringLen1 = strlen(inputString1); // Determina la lunghezza
	printf("\n");
	char* inputString2 = malloc(512);
	printf("Inserire seconda parola senza spazi: ");
	scanf("%s", inputString2);
	int stringLen2 = strlen(inputString2); // Determina la lunghezza
	printf("\n");
	// INVIO DATI AL SERVER
	if (send(Csocket, inputString1, stringLen1, 0) != stringLen1) {
		ErrorHandler("send() sent a different number of bytes than expected\n");
		closesocket(Csocket);
		ClearWinSock();
		system("PAUSE");
		return 0;
	}
	if (send(Csocket, inputString2, stringLen2, 0) != stringLen2) {
		ErrorHandler("send() sent a different number of bytes than expected\n");
		closesocket(Csocket);
		ClearWinSock();
		system("PAUSE");
		return 0;
	}
	// RICEZIONE DATI DAL SERVER
	char recvd1[512]="";
	int nbytes1=recv(Csocket, recvd1, sizeof(recvd1), 0);
	recvd1[nbytes1]='\0';
	printf("Received: %s\n",recvd1);
	if(nbytes1!=strlen(recvd1)) {
		ErrorHandler("recv() received less number of bytes than expected.\n");
		printf("Received: << %s >>\n---Bytes: %d | Error: %d ---\n",recvd1, nbytes1, errno);
		closesocket(Csocket);
		ClearWinSock();
		system("PAUSE");
		return 0;
	}
	char recvd2[512]="";
	int nbytes2=recv(Csocket, recvd2, sizeof(recvd2), 0);
	recvd2[nbytes2]='\0';
	printf("Received: %s\n",recvd2);
	if(nbytes2!=strlen(recvd2)) {
		ErrorHandler("recv() received less number of bytes than expected.\n");
		printf("Received: << %s >>\n---Bytes: %d | Error: %d ---\n",recvd2, nbytes2, errno);
		closesocket(Csocket);
		ClearWinSock();
		system("PAUSE");
		return 0;
	}
	// CHIUSURA DELLA CONNESSIONE
	printf("Chiusura client...\n");
	closesocket(Csocket);
	ClearWinSock();
	printf("\n"); // Print a final line feed
	system("pause");
	return(0);

}
