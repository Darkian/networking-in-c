/*
 * TCP Client primo esonero.c
 *
 *  Created on: 20 ott 2017
 *      Author: Manosperta Luigi, matricola 652735
 */

#if defined WIN32
#include <winsock.h> //libreria per la gestione delle socket in C
#else
#define closesocket close
#include <sys/socket.h>
#include <arpa/inet.h> //protocolli Internet
#include <unistd.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define BUFFERSIZE 512

void ErrorHandler(char *errorMessage) {
	printf(errorMessage);
}

void ClearWinSock() {
#if defined WIN32
	WSACleanup();
#endif

}

typedef struct { //uso la struct come da traccia per trasferire stringhe in input e risultato della concatenazione
	char msg1[100];
	char msg2[100];
	char msg3[200];
} message;

int main(void) {
#if defined WIN32
	WSADATA wsaData;
	int iResult = WSAStartup(MAKEWORD(2 ,2), &wsaData);
	if (iResult != 0) {
		printf ("error at WSASturtup\n");
		system("PAUSE");
		return 0;
	}
#endif
	// CREAZIONE DELLA SOCKET CHE SI CONNETTERA' AL SERVER
	int Csocket;
	Csocket = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP); //la socket � basata sul protocollo TCP
	if (Csocket < 0) {
		ErrorHandler("socket creation failed.\n");
		closesocket(Csocket);
		ClearWinSock();
		system("PAUSE");
		return 0;
	}
	// COSTRUZIONE DELL�INDIRIZZO DEL SERVER
	char *ind;
	ind=(char *)malloc(15);
	int port=0;
	printf("Inserire l'indirizzo del server : ");
	scanf("%s",ind);
	printf("Inserire la porta del server: ");
	scanf("%d",&port);
	struct sockaddr_in sad; //socket del server
	memset(&sad, 0, sizeof(sad)); //relativo settaggio iniziale
	sad.sin_family = AF_INET; //protocollo Internet corrispondente all'Internet Protocol v4
	sad.sin_addr.s_addr = inet_addr(ind); //indirizzo IP del server inserito prima da stdin
	sad.sin_port = htons(port); //porta del server inserita prima da stdin
	// CONNESSIONE AL SERVER
	if (connect(Csocket, (struct sockaddr *)&sad, sizeof(sad)) < 0) {
		ErrorHandler("Failed to connect.\n" );
		closesocket(Csocket);
		ClearWinSock();
		system("PAUSE");
		return 0;
	}
	// RICEZIONE DELLA STRINGA DI AVVENUTA CONNESSIONE
	char connEst[30]="";
	int nbytesEst=recv(Csocket, connEst, sizeof(connEst)-1, 0);
	connEst[nbytesEst]='\0';
	printf("Received: %s\n",connEst); //e relativa stampa a video
	if(nbytesEst!=strlen(connEst)) {
		ErrorHandler("recv() received less number of bytes than expected.\n");
		printf("Received: << %s >>\n---Bytes: %d | Error: %d ---\n",connEst, nbytesEst, errno);
		closesocket(Csocket);
		ClearWinSock();
		system("PAUSE");
		return 0;
	}
	char *str1, *str2;
	message res;
	message msgs;
	do{
		//piccolo reset artigianale della memoria
		strcpy(res.msg1,"");
		strcpy(res.msg2,"");
		strcpy(res.msg3,"");
		str1=(char *)malloc(100);
		str2=(char *)malloc(100);
		do{
			printf("Inserire prima parola: ");
			scanf("%s",str1);
		}while(strlen(str1)>100);
		do{
			printf("Inserire seconda parola: ");
			scanf("%s",str2);
		}while(strlen(str2)>100);
		//piccolo reset artigianale della memoria come prima, ma leggermente pi� sofisticato
		int i;
		for(i=0;i<=strlen(str1);i++)
			msgs.msg1[i]=str1[i];
		msgs.msg1[i+1]='\0';
		for(i=0;i<=strlen(str2);i++)
			msgs.msg2[i]=str2[i];
		msgs.msg2[i+1]='\0';
		msgs.msg3[0]='\0';
		//INVIO TRIPLA DI STRINGHE [...] AL SERVER
		if (send(Csocket, (void *)&msgs, sizeof(msgs), 0)<0) {
			ErrorHandler("send() sent a different number of bytes than expected\n");
			closesocket(Csocket);
			ClearWinSock();
			system("PAUSE");
			return 0;
		}
		// RICEZIONE TRIPLA DI STRINGHE [...] DAL SERVER
		int nbytes=recv(Csocket, (void *)&res, sizeof(msgs), 0);
		if(nbytes>sizeof(res)) {
			ErrorHandler("La recv() della struct ha ricevuto meno bytes del previsto.\n");
			closesocket(Csocket);
			ClearWinSock();
			system("PAUSE");
			return 0;
		}
		printf("Il risultato della concatenazione e': %s\n", res.msg3);
		//piccolo rilascio della memoria per poter ri-eseguire il ciclo senza avere celle di memoria "dirty"
		free(str1);
		free(str2);
		strcpy(msgs.msg1,"");
		strcpy(msgs.msg2,"");
		strcpy(msgs.msg3,"");
	}while(strcmp(res.msg3,"bye")!=0);
	// CHIUSURA DELLA CONNESSIONE
	printf("Chiusura client...\n");
	closesocket(Csocket);
	ClearWinSock();
	printf("\n");
	system("pause");
	free(ind);
	return EXIT_SUCCESS;
}

