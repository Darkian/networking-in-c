#if defined WIN32
#include <winsock.h>
#else
#define closesocket close
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#endif
#include <stdio.h>
#include <stdlib.h> // for atoi()
#include <string.h>
#include <ctype.h>
#define PROTOPORT 5193 // default protocol port number
#define QLEN 6 // size of request queue
void ErrorHandler(char *errorMessage) {
	printf(errorMessage);
}
void ClearWinSock() {
#if defined WIN32
	WSACleanup();
#endif
}
int main(int argc, char *argv[]) {
	int port;
	if (argc > 1) {
		port = atoi(argv[1]); // if argument specified convert argument to binary
	}
	else
		port = PROTOPORT; // use default port number
	if (port < 0) {
		printf("bad port number %s \n", argv[1]);
		return 0;
	}
#if defined WIN32 // initialize Winsock
	WSADATA wsaData;
	int iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
	if (iResult != 0) {
		ErrorHandler("Error at WSAStartup()\n");
		return 0;
	}
#endif
	int MySocket;
	MySocket = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (MySocket < 0) {
		ErrorHandler("socket creation failed.\n");
		ClearWinSock();
		system("PAUSE");
		return 0;
	}
	// ASSEGNAZIONE DI UN INDIRIZZO ALLA SOCKET
	struct sockaddr_in sad;
	memset(&sad, 0, sizeof(sad)); // ensures that extra bytes contain 0
	sad.sin_family = AF_INET;
	sad.sin_addr.s_addr = inet_addr("127.0.0.1");
	sad.sin_port = htons(port); /* converts values between the host and
	network byte order. Specifically, htons() converts 16-bit quantities
	from host byte order to network byte order. */
	if (bind(MySocket, (struct sockaddr*) &sad, sizeof(sad)) < 0) {
		ErrorHandler("bind() failed.\n");
		closesocket(MySocket);
		ClearWinSock();
		system("PAUSE");
		return 0;
	}
	if (listen (MySocket, QLEN) < 0) {
		ErrorHandler("listen() failed.\n");
		closesocket(MySocket);
		ClearWinSock();
		system("PAUSE");
		return 0;
	}
	// ACCETTARE UNA NUOVA CONNESSIONE
	struct sockaddr_in cad; // structure for the client address
	int clientSocket; // socket descriptor for the client
	int clientLen; // the size of the client address
	printf("Waiting for a client to connect...\n");
	while (1) { /* oppure for (;;) */
		clientLen = sizeof(cad); // set the size of the client address
		clientSocket = accept(MySocket, (struct sockaddr *)&cad, &clientLen);
		if (clientSocket < 0) {
			ErrorHandler("accept() failed.\n");
			closesocket(MySocket);
			ClearWinSock();
			system("PAUSE");
			return 0;
		}
		printf("Connection established! Handling client %s\n", inet_ntoa(cad.sin_addr));
		char* connEst = malloc(20);
		connEst = "connessione avvenuta";
		int lenConn = strlen(connEst);
		if (send(clientSocket, connEst, lenConn, 0) != lenConn) {
			ErrorHandler("send() sent a different number of bytes than expected\n");
			closesocket(clientSocket);
			ClearWinSock();
			system("PAUSE");
			return 0;
		}
		// RICEZIONE DATI DAL CLIENT
		char recvd1[512]="";
		int nbytes1=recv(clientSocket, recvd1, sizeof(recvd1)-1, 0);
		recvd1[nbytes1]='\0';
		printf("Received: %s\n",recvd1);
		if(nbytes1!=strlen(recvd1)) {
			ErrorHandler("recv() received less number of bytes than expected.\n");
			printf("Received: << %s >>\n---Bytes: %d | Error: %d ---\n",recvd1, nbytes1, errno);
			closesocket(MySocket);
			ClearWinSock();
			system("PAUSE");
			return 0;
		}
		char recvd2[512]="";
		int nbytes2=recv(clientSocket, recvd2, sizeof(recvd2)-1, 0);
		recvd2[nbytes2]='\0';
		printf("Received: %s\n",recvd2);
		if(nbytes2!=strlen(recvd2)) {
			ErrorHandler("recv() received less number of bytes than expected.\n");
			printf("Received: << %s >>\n---Bytes: %d | Error: %d ---\n",recvd2, nbytes2, errno);
			closesocket(MySocket);
			ClearWinSock();
			system("PAUSE");
			return 0;
		}
		// INVIO DATI AL CLIENT
		char* res1 = recvd1;
		for(int i=0;recvd1[i]!='\0';i++)
			res1[i]=toupper(recvd1[i]);
		char* res2 = recvd2;
		for(int i=0;recvd2[i]!='\0';i++)
			res2[i]=tolower(recvd2[i]);
		int stringLen1 = strlen(res1), stringLen2 = strlen(res2); // Determina la lunghezza
		// INVIARE DATI AL SERVER1
		if (send(clientSocket, res1, stringLen1, 0) != stringLen1) {
			ErrorHandler("send() sent a different number of bytes than expected\n");
			closesocket(clientSocket);
			ClearWinSock();
			system("PAUSE");
			return 0;
		}
		if (send(clientSocket, res2, stringLen2, 0) != stringLen2) {
			ErrorHandler("send() sent a different number of bytes than expected\n");
			closesocket(clientSocket);
			ClearWinSock();
			system("PAUSE");
			return 0;
		}
	}
}
