#if defined WIN32
#include <winsock.h>
#else
#define closesocket close
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#define BUFFERSIZE 512
#define PROTOPORT 5193 // Numero di porta di default

void ErrorHandler(char *errorMessage) {
	printf(errorMessage);
}

void ClearWinSock() {
#if defined WIN32
	WSACleanup();
#endif

}

int main(void) {
#if defined WIN32
	WSADATA wsaData;
	int iResult = WSAStartup(MAKEWORD(2 ,2), &wsaData);
	if (iResult != 0) {
		printf ("error at WSASturtup\n");
		system("PAUSE");
		return 0;
	}
#endif
	// CREAZIONE DELLA SOCKET SERVER
	int Csocket;
	Csocket = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (Csocket < 0) {
		ErrorHandler("socket creation failed.\n");
		closesocket(Csocket);
		ClearWinSock();
		system("PAUSE");
		return 0;
	}
	// COSTRUZIONE DELL�INDIRIZZO DEL SERVER
	char *ind;
	ind=malloc(15);
	int port=0;
	printf("Inserire l'indirizzo del server : ");
	scanf("%s",ind);
	printf("Inserire la porta del server: ");
	scanf("%d",&port);
	struct sockaddr_in sad;
	memset(&sad, 0, sizeof(sad));
	sad.sin_family = AF_INET;
	sad.sin_addr.s_addr = inet_addr(ind); // IP del server
	sad.sin_port = htons(port); // Server port
	// CONNESSIONE AL SERVER
	if (connect(Csocket, (struct sockaddr *)&sad, sizeof(sad)) < 0) {
		ErrorHandler("Failed to connect.\n" );
		closesocket(Csocket);
		ClearWinSock();
		system("PAUSE");
		return 0;
	}
	if(send(Csocket, "Hello", strlen("Hello"), 0)<0) { // la prima met� del "two-way handshaking"
		ErrorHandler("Transmitting 'Hello' didn't work properly...\n");
		closesocket(Csocket);
		ClearWinSock();
		system("PAUSE");
		return 0;
	}
	// RICEZIONE DELLA STRINGA DI AVVENUTA CONNESSIONE
	char connEst[10]="";
	int nbytesEst=recv(Csocket, connEst, sizeof(connEst)-1, 0);
	connEst[nbytesEst]='\0';
	printf("Received: %s\n",connEst);
	if(nbytesEst!=strlen(connEst)) {
		ErrorHandler("recv() received less number of bytes than expected.\n");
		printf("Received: << %s >>\n---Bytes: %d | Error: %d ---\n",connEst, nbytesEst, errno);
		closesocket(Csocket);
		ClearWinSock();
		system("PAUSE");
		return 0;
	}
	int n1=0,n2=0;
	printf("Inserire primo numero: ");
	scanf("%d",&n1);
	printf("\nInserire secondo numero: ");
	scanf("%d",&n2);
	printf("\n");
	char* num1=malloc(sizeof(int)*sizeof(n1));
	char* num2=malloc(sizeof(int)*sizeof(n2));
	snprintf(num1, sizeof(num1)*sizeof(int), "%d", n1);
	snprintf(num2, sizeof(num2)*sizeof(int), "%d", n2);
	// INVIO DATI AL SERVER
	if (send(Csocket, num1, sizeof(num1), 0)<0) {
		ErrorHandler("first send() sent a different number of bytes than expected\n");
		closesocket(Csocket);
		ClearWinSock();
		system("PAUSE");
		return 0;
	}
	if (send(Csocket, num2, sizeof(num2), 0)<0) {
		ErrorHandler("second send() sent a different number of bytes than expected\n");
		closesocket(Csocket);
		ClearWinSock();
		system("PAUSE");
		return 0;
	}
	// RICEZIONE DATI DAL SERVER
	char res[1000]="";
	int nbytes=recv(Csocket, res, sizeof(res), 0);
	res[nbytes]='\0';
	if(nbytes>sizeof(res)) {
		ErrorHandler("La recv() della somma ha ricevuto meno bytes del previsto.\n");
		closesocket(Csocket);
		ClearWinSock();
		system("PAUSE");
		return 0;
	}
	int ris=(int)strtol(res,NULL,10);
	printf("Received sum: %d.\n", ris);
	// CHIUSURA DELLA CONNESSIONE
	printf("Chiusura client...\n");
	closesocket(Csocket);
	ClearWinSock();
	printf("\n"); // Print a final line feed
	system("pause");
	return(0);

}
